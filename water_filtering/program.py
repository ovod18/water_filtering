import sqlite3

try:
    con = sqlite3.connect('db/test1.db')
    cur = con.cursor()
    print("Connection established.")

    cur.execute("""SELECT filtration_quality.date, filter_name.name, 
                   filtration_quality.value FROM filtration_quality 
                   INNER JOIN filter ON filter_id = filter.id 
                   INNER JOIN filter_name on filter.id = filter_name.id
                """)
    for row in cur.fetchall():
        print(row)

    con.commit()
    cur.close()
except sqlite3.Error as error:
    print("An error occurred while working with sqlite. ", error)
finally:
    try:
        if con:
            print("Total rows changed after connecting to the database: ",
                                                         con.total_changes)
            con.close()
            print("Connection closed.")
    except NameError as error:
        print("The connection was not established.")

