"""This module contains functions for work with tables data.

:platform: Linux
:author: Ovod18

FUNCTIONS

:py:func:`.create_list`

:py:func:`.create_dict`

:py:func:`.get_columns`

:py:func:`.make_table`

|
"""
from prettytable import PrettyTable
import myquery as mq

def create_list(cur):
    """This function creates the list of table names.

    :param: cur: The cursor object in the connection to database.
    :return: tablist: list

    |
    """
    cur.execute(mq.get_all_tables())
    tablist = []
    for row in cur.fetchall():
        if row[1].find("sqlite_"):
            tablist.append(row[1])
    return tablist

def create_dict(cur):
    """This function creates the dict of table names.

    :param: cur: The cursor object in the connection to database.
    :return: tabdict: dict

    |
    """
    tablist = create_list(cur)
    tabdict = {tablist[i]: tablist[i] for i in range(len(tablist))}
    print("tabdict:", tabdict)

def get_columns(cur, table = None):
    """This function creates a list of columns names. If table table is None, 
       then uses last builded table in cur.

    :param: cur: The cursor object in the connection to database.
    :param: table: A table for get columns.
    :return: tuple: tuple of columns.

    |
    """
    if table: cur.execute(mq.select_all_from(table, limit = 1))
    return tuple((i[0] for i in cur.description))

def make_table(cur, fields):
    """Makes table.

    :param: cur: The cursor object in the connection to database.
    :param: table_header: Header of table being maked.
    :return: table: PrettyTable object.

    |
    """
    table = PrettyTable(fields)
    for row in cur.fetchall():
        table.add_row(row)
    table.align = "l"
    return table
