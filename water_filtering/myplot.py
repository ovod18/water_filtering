"""This module contains plots and functions for work with it.

:platform: Linux
:author: Ovod18

FUNCTIONS

:py:func:`.make_plot`

:py:func:`.transform_date`

|
"""
import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

row_dates = ("2022-07-06", "2022-07-12", "2022-07-19")
values = (220, 207, 225)

row_date = "2022-07-06"


def transform_date(row_date):
    """Parsing dates tuple.

    :param: row_date: the date as a string ('1990-07-23').
    :type: dates: tuple

    |
    """
    splitted_date = row_date.split('-')
    zeroless_date = (item.lstrip('0') for item in splitted_date)
    int_date = tuple((int(item) for item in zeroless_date))
    date = datetime.date(*int_date)
    return date

def make_plot(filter_name, dates, values):
    """This function makes the plot with given dates and values.

    :param: dates: the tuple of dates.
    :type: dates: tuple
    :param: values: the tuple of values.
    :type: values: tuple

    |
    """
    months = mdates.MonthLocator()
    auto = mdates.AutoDateLocator()
    days = mdates.DayLocator()
    weeks = mdates.WeekdayLocator()
    timeFmt = mdates.DateFormatter('%d-%m-%y')
    #dates = (datetime.date(2022,7,1),
    #         datetime.date(2022,8,31),
    #         datetime.date(2022,9,14))
    dates = dates
    values = values
    fig, ax = plt.subplots()
    plt.plot(dates, values, 'b-o')
    plt.title(filter_name)
    plt.xlabel('Date')
    plt.ylabel('Value')
    ax.xaxis.set_major_locator(weeks)
    ax.xaxis.set_major_formatter(timeFmt)
    ax.xaxis.set_minor_locator(days)
    plt.grid(True)
    plt.show()


#dates = tuple((transform_date(r_date) for r_date in row_dates))
#make_plot(dates, values)
