.. watwer_filtering documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to space_shooter's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules 

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
