import sqlite3, tables, commands
import myquery as mq

try:
    con = sqlite3.connect('db/w_f_base.db')
    cur = con.cursor()
    print("Connection established.")

    while True:
        commands.execute(cur, commands.input_command())

    con.commit()
    cur.close()
except sqlite3.Error as error:
    print("An error occurred while working with sqlite. ", error)
finally:
    try:
        if con:
            print("Total rows changed after connecting to the database:",
                                                         con.total_changes)
            con.close()
            print("Connection closed.")
    except NameError as error:
        print("The connection was not established.")

