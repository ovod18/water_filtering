"""This module contains querys for database.

:platform: Linux
:author: Ovod18

FUNCTIONS

:py:func:`.select_all_from`

:py:func:`.get_all_tables`

:py:func:`.execute`

|
"""

def execute(cur, query, filter_name=None):
    """This function execute a query for the database.

    :param: query: SQL query.
    :rtype: str.

    :param: filter_name: The filter name or part of it.
    :rtype: str.

    |
    """
    if filter_name:
        query = query + "WHERE filter_name.name LIKE " + \
                r"'%" + f'{filter_name}' + r"%'"
    cur.execute(query)

def select_all_from(table, limit = 0):
    """The query for get all data in table.

    :param: table: Table name.
    :return: query: SQL query.
    :rtype: str.

    |
    """
    if not limit: return (f"SELECT * FROM {table}")
    else: return (f"SELECT * FROM {table} LIMIT {limit}")

def get_all_tables():
    """The query for get all data in table.

    :return: query: SQL query.
    :rtype: str.

    |
    """
    return("SELECT * FROM sqlite_master WHERE type = 'table'")

osmos_query = """SELECT filtration_quality.date, filter_name.name, filter.id, 
                 filtration_quality.value FROM filtration_quality 
                 INNER JOIN filter ON filter_id = filter.id 
                 INNER JOIN filter_name on filter.id = filter_name.id 
                 WHERE filter_name.name LIKE '%osm%'
              """

quality_query = """SELECT filtration_quality.date, filter_name.name, filter.id, 
                 filtration_quality.value FROM filtration_quality 
                 INNER JOIN filter ON filter_id = filter.id 
                 INNER JOIN filter_name on filter.name_id = filter_name.id 
              """

filter_query = """SELECT filter.id, filter_name.name, filter.installation_date, 
                  filter.'lifetime(years)', filter.'resource(liters)' 
                  FROM filter 
                  INNER JOIN filter_name on filter.name_id = filter_name.id 
              """

