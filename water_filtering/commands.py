"""This module contains users commands and its implementation.

:platform: Linux
:author: Ovod18

FUNCTIONS

:py:func:`.input_command`

:py:func:`.execute`

|
"""
import tables
import myquery as mq
import myplot

def input_command():
    """Input user command.

    :return: text: Row text of user command.
    :rtype: str

    |
    """
    prompt = "Enter type of query: "
    return input(prompt)

def execute(cur, command):
    """Interpretation user command for its next executing.

    :param: command: user command text.
    :type: command: str
    :param: cur: the database cursor.

    |
    """
    if command == "quality":
        try:
            filter_name = input("Enter filter name" +
                                " or leave it empty for all filters: ")
            mq.execute(cur, mq.quality_query, filter_name=filter_name)
            fields = ("date", "filter name", "filter id", "value")
            print(tables.make_table(cur, fields))

            if filter_name:
                mq.execute(cur, mq.quality_query, filter_name=filter_name)
                dates = tuple((myplot.transform_date(row[0])
                               for row in cur.fetchall()))
                mq.execute(cur, mq.quality_query, filter_name=filter_name)
                values = tuple((row[3] for row in cur.fetchall()))
                myplot.make_plot(filter_name, dates, values)
        except IndexError as error:
            mq.execute(cur, mq.quality_query)

    elif command == "filter":
        try:
            filter_name = input("Enter filter name" +
                                " or leave it empty for all filters: ")
            mq.execute(cur, mq.filter_query, filter_name=filter_name)
            fields = ("filter id", "filter name", "installation date",
                      "lifetime(years)", "resource(liters)")
            print(tables.make_table(cur, fields))
        except IndexError as error:
            mq.execute(cur, mq.main_query)

    elif not command: pass
    else: print("Command not found.")
