import sqlite3, tables, commands
import tkinter as tk
import myquery as mq

def db_connect():
    try:
     con = sqlite3.connect('db/w_f_base.db')
     cur = con.cursor()
     print("Connection established.")
     while True:
         commands.execute(cur, commands.input_command())

     con.commit()
     cur.close()
    except sqlite3.Error as error:
     print("An error occurred while working with sqlite. ", error)
    finally:
     try:
         if con:
             print("Total rows changed after connecting to the database:",
                                                          con.total_changes)
             con.close()
             print("Connection closed.")
     except NameError as error:
         print("The connection was not established.")

root = tk.Tk()
root.title("water_filtering")


frame1 = tk.Frame(master = root, bg = "black")
frame1.pack()

connection_btn = tk.Button(master = frame1,
                           text = "Connect to db",
                           command = db_connect,
                           width = 24,
                           height = 3,
                           bg = "blue",
                           fg = "yellow")
connection_btn.pack()



root.mainloop()
